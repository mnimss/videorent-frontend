package ru.nstu.abramov.videorent.front.view;

import ru.nstu.abramov.videorent.front.dto.Movie;
import ru.nstu.abramov.videorent.front.dto.Transaction;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class TablePanel extends JPanel {

    private TransactionModel tableModel;
    private JScrollPane transactionScroll;

    private MovieModel movieModel;
    private JScrollPane movieScroll;

    {
        setLayout(null);
    }

    public TablePanel() {

        initTableTransactions();
        initTableMovies();
    }

    private void initTableMovies() {

        movieModel = new MovieModel();

        JTable table = new JTable(movieModel);
        movieScroll = new JScrollPane(table);
        movieScroll.setBounds(20, 20, 1000, 700);
        table.setBounds(20, 20, 1000, 700);
        sizingColumnMovie(table);

        movieScroll.setVisible(false);

        add(movieScroll);
    }

    private void initTableTransactions() {

        tableModel = new TransactionModel();

        JTable table = new JTable(tableModel);
        transactionScroll = new JScrollPane(table);
        transactionScroll.setBounds(20, 20, 1000, 700);
        table.setBounds(20, 20, 1000, 700);
        sizingColumnTransaction(table);

        add(transactionScroll);
    }

    private void sizingColumnTransaction(JTable table) {

        table.getColumnModel().getColumn(0).setMaxWidth(130);
        table.getColumnModel().getColumn(1).setMaxWidth(80);
        table.getColumnModel().getColumn(2).setMaxWidth(300);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setMaxWidth(50);
        table.getColumnModel().getColumn(5).setMaxWidth(150);
        table.getColumnModel().getColumn(6).setMaxWidth(150);
        table.getColumnModel().getColumn(7).setMaxWidth(220);
    }

    private void sizingColumnMovie(JTable table){

        table.getColumnModel().getColumn(0).setMaxWidth(300);
        table.getColumnModel().getColumn(1).setMaxWidth(300);
        table.getColumnModel().getColumn(2).setMaxWidth(300);
    }

    public void updateTableTransaction(List<Transaction> transactions){

        movieScroll.setVisible(false);
        transactionScroll.setVisible(true);

        tableModel.setTransactions(transactions);
        tableModel.fireTableDataChanged();
    }

    public void updateTableMovie(List<Movie> movies){

        transactionScroll.setVisible(false);
        movieScroll.setVisible(true);

        movieModel.setMovies(movies);
        movieModel.fireTableDataChanged();
    }
}

class TransactionModel extends AbstractTableModel{

    private List<Transaction> transactions = new ArrayList<>();

    public TransactionModel() {
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int getRowCount() {
        return transactions.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Transaction transaction = transactions.get(rowIndex);

        switch (columnIndex){
            case 0 : {
                return transaction.getId();
            }
            case 1 : {
                return transaction.getCustomer().getLastName();
            }
            case 2 : {
                return transaction.getMovie().getName();
            }
            case 3 : {
                return transaction.getMovie().getGenre();
            }
            case 4 : {
                return transaction.getMovie().getPrice();
            }
            case 5 : {
                return transaction.getTakeTime();
            }
            case 6 : {
                return transaction.getResetTime();
            }
            case 7 : {
                return transaction.getRealResetTime();
            }
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column) {

         switch (column){
             case 0 : return "Номер транзакции";
             case 1 : return "Фамилия";
             case 2 : return "Название фильма";
             case 3 : return "Жанр";
             case 4 : return "Цена";
             case 5 : return "Дата выдачи";
             case 6 : return "Дата возврата";
             case 7 : return "Реальная дата возврата";
             default: return "";
         }
    }
}

class MovieModel extends AbstractTableModel{

    private List<Movie> movies = new ArrayList<>();

    public MovieModel() {
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public int getRowCount() {
        return movies.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Movie movie = movies.get(rowIndex);

        switch (columnIndex){
            case 0 : {
                return movie.getName();
            }
            case 1 : {
                return movie.getGenre();
            }
            case 2 : {
                return movie.getPrice();
            }
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column){
            case 0 : return "Название фильма";
            case 1 : return "Жанр";
            case 2 : return "Цена";
            default: return "";
        }
    }
}