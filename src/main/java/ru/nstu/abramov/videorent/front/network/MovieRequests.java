package ru.nstu.abramov.videorent.front.network;

import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;
import ru.nstu.abramov.videorent.front.dto.Movie;

import java.util.List;
import java.util.Map;

public interface MovieRequests {

    @RequestLine("POST /")
    @Headers("Content-Type: application/json")
    void createMovie(Movie movie);

    @RequestLine("GET /name/{name}")
    Movie provideMovie(@Param("name") String name);

    @RequestLine("GET /")
    List<Movie> provideAllMovies(@QueryMap Map<String, String> filters);
}