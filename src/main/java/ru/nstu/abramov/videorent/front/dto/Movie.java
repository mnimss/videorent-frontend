package ru.nstu.abramov.videorent.front.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Movie implements Serializable {

    private long id;
    private String name;
    private String genre;
    private int price;
    private transient List<Transaction> transactions;

    public Movie() {
    }

    public Movie(String name, String genre, int price, List<Transaction> transactions) {
        this.name = name;
        this.genre = genre;
        this.price = price;
        this.transactions = transactions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return getPrice() == movie.getPrice() &&
                getName().equals(movie.getName()) &&
                getGenre().equals(movie.getGenre());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getGenre(), getPrice());
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", price=" + price +
                '}';
    }
}