package ru.nstu.abramov.videorent.front.view;

import ru.nstu.abramov.videorent.front.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private Controller controller;
    private Menu menu;
    private TablePanel tablePanel;
    private ActionPanel actionPanel;
    private int width;
    private int height;

    public MainFrame(Controller controller) {
        this.controller = controller;

        initFrame();
        initComponents();
    }

    private void initComponents() {

        tablePanel = new TablePanel();
        tablePanel.setPreferredSize(new Dimension(1040, height));
        tablePanel.setVisible(true);
        add(tablePanel, BorderLayout.WEST);

        actionPanel = new ActionPanel(controller, this);
        actionPanel.setPreferredSize(new Dimension(360, height));
        actionPanel.setVisible(true);
        add(actionPanel, BorderLayout.EAST);

        JMenuBar menuBar = new JMenuBar();
        menu = new Menu(controller, this);
        menuBar.add(menu);
        menuBar.setVisible(true);
        setJMenuBar(menuBar);
    }

    private void initFrame() {

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Электронная система учета работы пункта проката видео");
        setLayout(new BorderLayout());

        width = 1400;
        height = 800;
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);
    }

    public TablePanel getTablePanel() {
        return tablePanel;
    }
}