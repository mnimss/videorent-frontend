package ru.nstu.abramov.videorent.front.view;

import ru.nstu.abramov.videorent.front.controller.Controller;
import ru.nstu.abramov.videorent.front.dto.Customer;
import ru.nstu.abramov.videorent.front.dto.Movie;

import javax.swing.*;
import java.awt.*;

public class Menu extends JMenu{

    private Controller controller;
    private MainFrame mainFrame;
    private JMenuItem newCustomer;
    private JMenuItem newMovie;
    private JMenuItem profit;

    public Menu(Controller controller, MainFrame mainFrame) {

        super("Новый");

        this.controller = controller;
        this.mainFrame = mainFrame;

        initComponents();
    }

    private void initComponents() {

        initNewCustomer();
        initNewMovie();
        initProfit();
    }

    private void initProfit() {

        profit = new JMenuItem();
        profit.setText("Доход");
        add(profit);
        profit.setVisible(true);
        profit.addActionListener(e -> {

            JDialog dialog = new JDialog(mainFrame, "Доход", false);
            dialog.setLayout(null);

            int width = 400;
            int height = 250;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimension = toolkit.getScreenSize();
            dialog.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);

            JLabel from = new JLabel("Промежуток от");
            from.setBounds(40, 40, 130, 25);
            from.setVisible(true);
            dialog.add(from);

            JTextField textFrom = new JTextField(25);
            textFrom.setBounds(160, 40, 200, 25);
            textFrom.setVisible(true);
            dialog.add(textFrom);

            JLabel until = new JLabel("Промежуток до");
            until.setBounds(40, 80, 130, 25);
            until.setVisible(true);
            dialog.add(until);

            JTextField textUntil = new JTextField(25);
            textUntil.setBounds(160, 80, 200, 25);
            textUntil.setVisible(true);
            dialog.add(textUntil);

            JLabel result = new JLabel("Результат");
            result.setBounds(40, 120, 130, 25);
            result.setVisible(true);
            dialog.add(result);

            JTextField textResult = new JTextField(25);
            textResult.setBounds(160, 120, 200, 25);
            textResult.setEditable(false);
            textResult.setVisible(true);
            dialog.add(textResult);

            JButton calculate = new JButton("Вычислить");
            calculate.setBounds(160, 160, 140, 30);
            dialog.add(calculate);
            calculate.addActionListener(l -> {

                int resultValue = controller.getProfit(textFrom.getText(), textUntil.getText());
                textResult.setText(String.valueOf(resultValue));
            });

            dialog.setVisible(true);
        });
    }

    private void initNewMovie() {

        newMovie = new JMenuItem("Новый фильм");
        add(newMovie);
        newMovie.setVisible(true);
        newMovie.addActionListener(e -> {

            JDialog dialog = new JDialog(mainFrame, "Новый фильм", false);
            dialog.setLayout(null);

            int width = 400;
            int height = 250;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimension = toolkit.getScreenSize();
            dialog.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);

            JLabel name = new JLabel("Название фильма");
            name.setBounds(40, 40, 130, 25);
            name.setVisible(true);
            dialog.add(name);

            JTextField textName = new JTextField(25);
            textName.setBounds(160, 40, 200, 25);
            textName.setVisible(true);
            dialog.add(textName);

            JLabel genre = new JLabel("Жанр");
            genre.setBounds(40, 80, 130, 25);
            genre.setVisible(true);
            dialog.add(genre);

            JTextField textGenre = new JTextField(25);
            textGenre.setBounds(160, 80, 200, 25);
            textGenre.setVisible(true);
            dialog.add(textGenre);

            JLabel price = new JLabel("Цена");
            price.setBounds(40, 120, 130, 25);
            price.setVisible(true);
            dialog.add(price);

            JTextField textPrice = new JTextField(25);
            textPrice.setBounds(160, 120, 200, 25);
            textPrice.setVisible(true);
            dialog.add(textPrice);


            JButton ok = new JButton("OK");
            ok.setBounds(160, 160, 60, 30);
            dialog.add(ok);
            ok.addActionListener(l -> {

                Movie movie = new Movie();
                movie.setGenre(textGenre.getText());
                movie.setName(textName.getText());
                movie.setPrice(Integer.valueOf(textPrice.getText()));

                controller.createMovie(movie);
                controller.provideAllMovies(null);

                dialog.setVisible(false);
            });

            dialog.setVisible(true);
        });
    }

    private void initNewCustomer() {

        newCustomer = new JMenuItem("Новый клиент");
        add(newCustomer);
        newCustomer.setVisible(true);
        newCustomer.addActionListener(e -> {

            JDialog dialog = new JDialog(mainFrame, "Новый клиент", false);
            dialog.setLayout(new FlowLayout());

            int width = 500;
            int height = 100;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimension = toolkit.getScreenSize();
            dialog.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);

            JLabel lastNameLabel = new JLabel("Фамилия");
            lastNameLabel.setVisible(true);
            dialog.add(lastNameLabel);

            JTextField text = new JTextField(25);
            text.setVisible(true);
            dialog.add(text);

            JButton ok = new JButton("OK");
            dialog.add(ok);
            ok.addActionListener(l -> {

                Customer customer = new Customer();
                customer.setLastName(text.getText());

                controller.createCustomer(customer);

                dialog.setVisible(false);
            });

            dialog.setVisible(true);
        });
    }
}