package ru.nstu.abramov.videorent.front.network;

import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;
import ru.nstu.abramov.videorent.front.dto.Customer;
import ru.nstu.abramov.videorent.front.dto.Transaction;

import java.util.List;
import java.util.Map;

public interface CustomerRequests {

    @RequestLine("GET /lastName/{lastName}")
    Customer provideCustomer(@Param("lastName") String lastName);

    @RequestLine("POST /")
    @Headers("Content-Type: application/json")
    void createCustomer(Customer customer);

    @RequestLine("GET /lastName/{lastName}/transactions")
    List<Transaction> transactionsByCustomer(@Param("lastName") String lastName,
                                             @QueryMap Map<String, String> filters);
}