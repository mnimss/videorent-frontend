package ru.nstu.abramov.videorent.front.dto;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Objects;

public class Transaction implements Serializable {

    private long id;
    private String takeTime;
    private String resetTime;
    private String realResetTime;
    private Customer customer;
    private Movie movie;

    public Transaction() {
    }

    public Transaction(String takeTime, String resetTime, String realResetTime, Customer customer, Movie movie) {
        this.takeTime = takeTime;
        this.resetTime = resetTime;
        this.realResetTime = realResetTime;
        this.customer = customer;
        this.movie = movie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(String takeTime) {
        this.takeTime = takeTime;
    }

    public String getResetTime() {
        return resetTime;
    }

    public void setResetTime(String resetTime) {
        this.resetTime = resetTime;
    }

    public String getRealResetTime() {
        return realResetTime;
    }

    public void setRealResetTime(String realResetTime) {
        this.realResetTime = realResetTime;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return getTakeTime().equals(that.getTakeTime()) &&
                getResetTime().equals(that.getResetTime()) &&
                Objects.equals(getRealResetTime(), that.getRealResetTime()) &&
                getCustomer().equals(that.getCustomer()) &&
                getMovie().equals(that.getMovie());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTakeTime(), getResetTime(), getRealResetTime(), getCustomer(), getMovie());
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", takeTime=" + takeTime +
                ", resetTime=" + resetTime +
                ", realResetTime=" + realResetTime +
                ", customer=" + customer +
                ", movie=" + movie +
                '}';
    }

    public static DateTime convert(String dateTime){
        return DateTime.parse(dateTime);
    }
}