package ru.nstu.abramov.videorent.front.controller;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import ru.nstu.abramov.videorent.front.dto.Customer;
import ru.nstu.abramov.videorent.front.dto.Movie;
import ru.nstu.abramov.videorent.front.dto.Transaction;
import ru.nstu.abramov.videorent.front.network.CustomerRequests;
import ru.nstu.abramov.videorent.front.network.MovieRequests;
import ru.nstu.abramov.videorent.front.network.TransactionRequests;
import ru.nstu.abramov.videorent.front.view.MainFrame;

import java.util.*;

public class Controller {

    private MainFrame mainFrame;
    private CustomerRequests customerRequests;
    private MovieRequests movieRequests;
    private TransactionRequests transactionRequests;

    private List<Transaction> transactions = new ArrayList<>();
    private List<Movie> movies = new ArrayList<>();

    public Controller() {

        mainFrame = new MainFrame(this);
        customerRequests = Feign
                                .builder()
                                .decoder(new JacksonDecoder())
                                .encoder(new JacksonEncoder())
                                .target(CustomerRequests.class, "http://localhost:10000/customers");

        movieRequests = Feign
                                .builder()
                                .decoder(new JacksonDecoder())
                                .encoder(new JacksonEncoder())
                                .target(MovieRequests.class, "http://localhost:10000/movies");

        transactionRequests = Feign
                                .builder()
                                .decoder(new JacksonDecoder())
                                .encoder(new JacksonEncoder())
                                .target(TransactionRequests.class, "http://localhost:10000/transactions");
    }

    public void createCustomer(Customer customer){

        customerRequests.createCustomer(customer);
    }

    public void createMovie(Movie movie){

        movieRequests.createMovie(movie);
    }

    public int getProfit(String start, String end){

        Map<String, String> times = new HashMap<>();
        times.put("start", start);
        times.put("end", end);

        return transactionRequests.getProfit(times);
    }

    public Customer provideCustomer(String lastName){

        return customerRequests.provideCustomer(lastName);
    }

    public Movie provideMovie(String name){

        return movieRequests.provideMovie(name);
    }

    public void createTransaction(Transaction transaction){
        transactionRequests.createTransaction(transaction);

        transactions = getAllTransactions(null, null);
    }

    public List<Transaction> getAllTransactions(String takeTime, String isCorrect){

        Map<String, String> filters = new HashMap<>();
        filters.put("takeTime", takeTime);
        filters.put("isCorrect", isCorrect);

        transactions = transactionRequests.getAllTransactions(filters);

        mainFrame.getTablePanel().updateTableTransaction(transactions);

        return transactions;
    }

    public void closeTransaction(Transaction transaction){

        transactionRequests.close(transaction);
    }

    public void transactionsByCustomer(String lastName, String takeTime, String isCorrect){

        Map<String, String> filters = new HashMap<>();
        filters.put("takeTime", takeTime);
        filters.put("isCorrect", isCorrect);

        transactions = customerRequests.transactionsByCustomer(lastName, filters);

        mainFrame.getTablePanel().updateTableTransaction(transactions);
    }

    public List<Movie> provideAllMovies(String genre){

        Map<String, String> filters = new HashMap<>();
        filters.put("genre", genre);

        movies = movieRequests.provideAllMovies(filters);
        mainFrame.getTablePanel().updateTableMovie(movies);

        return movies;
    }
}