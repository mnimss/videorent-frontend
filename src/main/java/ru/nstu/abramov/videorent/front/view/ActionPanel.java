package ru.nstu.abramov.videorent.front.view;

import ru.nstu.abramov.videorent.front.controller.Controller;
import ru.nstu.abramov.videorent.front.dto.Movie;
import ru.nstu.abramov.videorent.front.dto.Transaction;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.List;

public class ActionPanel extends JPanel {

    private Controller controller;
    private MainFrame mainFrame;

    private String takeTime;
    private String isInTime;
    private String genre;

    {
        setLayout(null);
        setAutoscrolls(true);
    }

    public ActionPanel(Controller controller, MainFrame mainFrame) {
        this.controller = controller;
        this.mainFrame = mainFrame;

        initNewTransaction();
        initCloseTransaction();
        initShowAllTransactions();
        initShowAllMovies();
        initFindByLastName();
        initFindByMovie();
        initFilters();
    }

    private void initShowAllMovies() {

        JButton showAllMovies = new JButton("Все фильмы");
        showAllMovies.setBounds(175, 100, 150, 30);
        showAllMovies.setVisible(true);
        add(showAllMovies);
        showAllMovies.addActionListener(l -> {

            this.isInTime = null;
            this.takeTime = null;

            controller.provideAllMovies(null);
        });
    }

    private void initShowAllTransactions() {

        JButton showAllTransactions = new JButton("Все заказы");
        showAllTransactions.setBounds(15, 100, 150, 30);
        showAllTransactions.setVisible(true);
        add(showAllTransactions);
        showAllTransactions.addActionListener(l -> {

            this.genre = null;

            controller.getAllTransactions(this.takeTime, this.isInTime);
        });
    }

    private void initFilters() {

        JTextField takeTime = new JTextField(25);
        takeTime.setBounds(15, 280, 200, 25);
        takeTime.setVisible(true);
        takeTime.setToolTipText("Фильтр по дате выдачи");
        add(takeTime);

        JButton takeTimeBtn = new JButton("Фильтровать");
        takeTimeBtn.setBounds(215, 280, 130, 25);
        takeTimeBtn.setVisible(true);
        add(takeTimeBtn);
        takeTimeBtn.addActionListener(l -> {

            this.takeTime = takeTime.getText();
            controller.getAllTransactions(this.takeTime, this.isInTime);
        });

        JTextField genre = new JTextField(25);
        genre.setBounds(15, 340, 200, 25);
        genre.setVisible(true);
        genre.setToolTipText("Фильтр по жанру");
        add(genre);

        JButton genreBtn = new JButton("Фильтровать");
        genreBtn.setBounds(215, 340, 130, 25);
        genreBtn.setVisible(true);
        add(genreBtn);
        genreBtn.addActionListener(l -> {

            List<Movie> movies = controller.provideAllMovies(genre.getText());

            if (movies == null){
                genre.setText("");
            }
            else {
                this.genre = genre.getText();
            }
        });

        JCheckBox isInTime = new JCheckBox("Возвращён вовремя");
        isInTime.setBounds(15, 400, 200, 30);
        isInTime.setVisible(true);
        add(isInTime);
        isInTime.addActionListener(l -> {

            this.isInTime = String.valueOf(isInTime.isSelected());
            controller.getAllTransactions(this.takeTime, this.isInTime);
        });
    }

    private void initFindByMovie() {

        JTextField findByMovie = new JTextField(25);
        findByMovie.setBounds(15, 220, 200, 25);
        findByMovie.setVisible(true);
        add(findByMovie);

        JButton findByMovieBtn = new JButton("Поиск по фильму");
        findByMovieBtn.setBounds(215, 220, 140, 25);
        findByMovieBtn.setVisible(true);
        findByMovieBtn.addActionListener(l -> {

            Movie movie = controller.provideMovie(findByMovie.getText());
            mainFrame.getTablePanel().updateTableMovie(Collections.singletonList(movie));
        });
        add(findByMovieBtn);
    }

    private void initFindByLastName() {

        JTextField findByLastName = new JTextField(25);
        findByLastName.setBounds(15, 160, 200, 25);
        findByLastName.setVisible(true);
        add(findByLastName);

        JButton findByLastNameBtn = new JButton("Поиск по фамилии");
        findByLastNameBtn.setBounds(215, 160, 150, 25);
        findByLastNameBtn.setVisible(true);
        findByLastNameBtn.addActionListener(l -> {

            if (controller.provideCustomer(findByLastName.getText()) != null){
                controller.transactionsByCustomer(findByLastName.getText(), this.takeTime, this.isInTime);
            }
            else {
                findByLastName.setText("");
            }
        });
        add(findByLastNameBtn);
    }

    private void initCloseTransaction() {

        JButton closeTransaction = new JButton("Закрыть заказ");
        closeTransaction.setBounds(175, 40, 150, 30);
        closeTransaction.setVisible(true);
        add(closeTransaction);
        closeTransaction.addActionListener(l -> {

            JDialog dialog = new JDialog(mainFrame, "Закрыть заказ", false);
            dialog.setLayout(null);

            int width = 400;
            int height = 200;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimension = toolkit.getScreenSize();
            dialog.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);

            JLabel idTransaction = new JLabel("Номер заказа");
            idTransaction.setBounds(40, 40, 130, 25);
            idTransaction.setVisible(true);
            dialog.add(idTransaction);

            JTextField textIdTransaction = new JTextField(25);
            textIdTransaction.setBounds(160, 40, 200, 25);
            textIdTransaction.setVisible(true);
            dialog.add(textIdTransaction);

            JLabel realResetTime = new JLabel("Дата возврата");
            realResetTime.setBounds(40, 80, 130, 25);
            realResetTime.setVisible(true);
            dialog.add(realResetTime);

            JTextField textRealResetTime = new JTextField(25);
            textRealResetTime.setBounds(160, 80, 200, 25);
            textRealResetTime.setVisible(true);
            dialog.add(textRealResetTime);

            JButton ok = new JButton("OK");
            ok.setBounds(160, 120, 60, 30);
            dialog.add(ok);
            ok.addActionListener(a -> {

                Transaction transaction = new Transaction();
                transaction.setId(Long.parseLong(textIdTransaction.getText()));
                transaction.setRealResetTime(textRealResetTime.getText());

                controller.closeTransaction(transaction);
                controller.getAllTransactions(this.takeTime, this.isInTime);

                dialog.setVisible(false);
            });

            dialog.setVisible(true);
        });
    }

    private void initNewTransaction() {

        JButton newTransaction = new JButton("Новый заказ");
        newTransaction.setBounds(15, 40, 150, 30);
        newTransaction.setVisible(true);
        add(newTransaction);
        newTransaction.addActionListener(l -> {

            JDialog dialog = new JDialog(mainFrame, "Новый заказ", false);
            dialog.setLayout(null);

            int width = 400;
            int height = 500;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimension = toolkit.getScreenSize();
            dialog.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);

            JLabel lastName = new JLabel("Фамилия");
            lastName.setBounds(40, 40, 130, 25);
            lastName.setVisible(true);
            dialog.add(lastName);

            JTextField textLastName = new JTextField(25);
            textLastName.setBounds(160, 40, 200, 25);
            textLastName.setVisible(true);
            dialog.add(textLastName);
            textLastName.addActionListener(e -> {

                if (controller.provideCustomer(textLastName.getText()) == null){

                    JTextArea text = new JTextArea("Такого клиента не существует");
                    text.setVisible(true);
                    text.setEditable(false);

                    JOptionPane.showMessageDialog(dialog, text);
                    textLastName.setText("");
                }
            });

            JLabel genre = new JLabel("Жанр");
            genre.setBounds(40, 120, 130, 25);
            genre.setVisible(true);
            dialog.add(genre);

            JTextField textGenre = new JTextField(25);
            textGenre.setBounds(160, 120, 200, 25);
            textGenre.setEditable(false);
            textGenre.setVisible(true);
            dialog.add(textGenre);

            JLabel price = new JLabel("Цена");
            price.setBounds(40, 160, 130, 25);
            price.setVisible(true);
            dialog.add(price);

            JTextField textPrice = new JTextField(25);
            textPrice.setBounds(160, 160, 200, 25);
            textPrice.setEditable(false);
            textPrice.setVisible(true);
            dialog.add(textPrice);

            JLabel name = new JLabel("Название фильма");
            name.setBounds(40, 80, 130, 25);
            name.setVisible(true);
            dialog.add(name);

            JTextField textName = new JTextField(25);
            textName.setBounds(160, 80, 200, 25);
            textName.addActionListener(e -> {

                Movie movie = controller.provideMovie(textName.getText());

                if (movie == null){

                    JTextArea text = new JTextArea("Такого фильма не существует");
                    text.setVisible(true);
                    text.setEditable(false);

                    JOptionPane.showMessageDialog(dialog, text);
                    textName.setText("");
                    textGenre.setText("");
                    textPrice.setText("");
                }
                else {
                    textGenre.setText(movie.getGenre());
                    textPrice.setText(String.valueOf(movie.getPrice()));
                }
            });
            textName.setVisible(true);
            dialog.add(textName);


            JLabel takeTime = new JLabel("Дата выдачи");
            takeTime.setBounds(40, 200, 130, 25);
            takeTime.setVisible(true);
            dialog.add(takeTime);

            JTextField textTakeTime = new JTextField(25);
            textTakeTime.setBounds(160, 200, 200, 25);
            textTakeTime.setVisible(true);
            dialog.add(textTakeTime);
            textTakeTime.setToolTipText("Введите дату в формате: yyyy-mm-ddThh:mm");

            JLabel resetTime = new JLabel("Дата возврата");
            resetTime.setBounds(40, 240, 130, 25);
            resetTime.setVisible(true);
            dialog.add(resetTime);

            JTextField textResetTime = new JTextField(25);
            textResetTime.setBounds(160, 240, 200, 25);
            textResetTime.setVisible(true);
            dialog.add(textResetTime);
            textResetTime.setToolTipText("Введите дату в формате: yyyy-mm-ddThh:mm");

            JButton ok = new JButton("OK");
            ok.setBounds(160, 280, 60, 30);
            dialog.add(ok);
            ok.addActionListener(a -> {

                if (textName.getText() == null
                || textName.getText().equals("")
                || textLastName.getText() == null
                || textLastName.getText().equals("")
                || textTakeTime.getText() == null
                || textTakeTime.getText().equals("")
                || textResetTime.getText() == null
                || textResetTime.getText().equals("")){

                    JTextArea text = new JTextArea("Заполните все поля");
                    text.setVisible(true);
                    text.setEditable(false);

                    JOptionPane.showMessageDialog(dialog, text);
                }
                else {
                    Transaction transaction = new Transaction();
                    transaction.setCustomer(controller.provideCustomer(textLastName.getText()));
                    transaction.setMovie(controller.provideMovie(textName.getText()));
                    transaction.setResetTime(textResetTime.getText());
                    transaction.setTakeTime(textTakeTime.getText());

                    controller.createTransaction(transaction);

                    dialog.setVisible(false);
                }
            });

            dialog.setVisible(true);
        });
    }
}