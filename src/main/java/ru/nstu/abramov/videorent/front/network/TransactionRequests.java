package ru.nstu.abramov.videorent.front.network;

import feign.*;
import ru.nstu.abramov.videorent.front.dto.Transaction;

import java.util.List;
import java.util.Map;

public interface TransactionRequests {

    @RequestLine("GET /profit")
    @Headers("Content-Type: application/json")
    int getProfit(@QueryMap Map<String, String> times);

    @RequestLine("POST /")
    @Headers("Content-Type: application/json")
    void createTransaction(Transaction transaction);

    @RequestLine("GET /")
    List<Transaction> getAllTransactions(@QueryMap Map<String, String> filters);

    @RequestLine("POST /close")
    @Headers("Content-Type: application/json")
    void close(Transaction transaction);
}